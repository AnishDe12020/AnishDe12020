# Hi there <img src="https://raw.githubusercontent.com/MartinHeinz/MartinHeinz/master/wave.gif" width="30px">, my name is Anish De!!!

## I am a small YouTuber, Developer and a Student

My YouTube Channel - [Anish Tech Tutorials](https://youtube.com/anishtechtutorials)
![YouTube Channel Subscribers](https://img.shields.io/youtube/channel/subscribers/UCY-rVWTIsN-G7s81C89SzkA?label=YouTube%20Channel%20Subscribers&style=social)

---
### My skills (Expanding as I learn more!!!):

<img src="https://github.com/devicons/devicon/blob/master/icons/javascript/javascript-original.svg" alt="JavaScript" width="50" height="50"/> <img src="https://github.com/devicons/devicon/blob/master/icons/css3/css3-original.svg" alt="Css" width="50" height="50"/> <img src="https://github.com/devicons/devicon/blob/master/icons/react/react-original.svg" alt="React" width="50" height="50"/> <img src="https://github.com/devicons/devicon/blob/master/icons/python/python-original.svg" alt="Python" width="50" height="50"/> <img src="https://github.com/devicons/devicon/blob/master/icons/github/github-original.svg" alt="GitHub" width="50" height="50"/> <img src="https://github.com/devicons/devicon/blob/master/icons/git/git-original.svg" alt="Git" width="50" height="50"/> <img src="https://github.com/devicons/devicon/blob/master/icons/linux/linux-original.svg" alt="Linux" width="50" height="50"/> <img src="https://github.com/devicons/devicon/blob/master/icons/vscode/vscode-original.svg" alt="VsCode" width="50" height="50"/> 

---

![Anish's GitHub Stats](https://my-readme-stats-anishde12020.vercel.app/api?username=AnishDe12020&theme=radical&show_icons=true&count_private=true)

![Top Langs](https://my-readme-stats-anishde12020.vercel.app/api/top-langs/?username=AnishDe12020&theme=radical&langs_count=10&layout=compact)

![Anish's wakatime stats](https://my-readme-stats-anishde12020.vercel.app/api/wakatime/?username=AnishDe12020&theme=radical&layout=compact)

---
